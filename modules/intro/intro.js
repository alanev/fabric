module.exports = {
    scope: document.$('.intro'),
    active: 0,
    activeClass: 'intro__slide--active',
    activeDotClass: 'intro__dot--active',
    lister () {
        
    },
    change (index, method = 'add') {
        this.slides.item(index).classList[method](this.activeClass)
        this.dots.item(index).classList[method](this.activeDotClass)
    },
    toggle (index) {
        this.change(this.active, 'remove')
        this.active = index
        this.change(this.active)
    },
    listen () {
        let self = this
        
        this.prev.on('click', e => {
            let index = self.active - 1
            if (index < 0) index = self.slides.length - 1
            self.toggle(index)
        })
        
        this.next.on('click', e => {
            let index = self.active + 1
            if (index >= self.slides.length) index = 0
            self.toggle(index)
        })
        
        this.dots.forEach((dot, index) => {
            dot.on('click', e => {
                self.toggle(index);
            })
        })
    },
    init () {
        let {scope} = this;
        if (scope) {
            this.slides = scope.$$('.intro__slide')
            this.prev = scope.$('.intro__arrow--prev')
            this.next = scope.$('.intro__arrow--next')
            this.dots = scope.$$('.intro__dot')
            
            this.toggle(0)
            this.listen()
        }
    }
}