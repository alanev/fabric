const nouislider = require('nouislider');
const slider = {
    scope: document.$('.slider'),
    init () {
        let {scope} = this;
        if (scope) {
            let min = parseInt(scope.dataset.min) || 0,
                max = parseInt(scope.dataset.max) || 1000,
                range = scope.$('.slider__range'),
                fields = [
                    scope.$('.slider__field--min'),
                    scope.$('.slider__field--max')
                ];
            
            if (range) {
                nouislider.create(range, {
                    start: [min, max],
                    range: {
                        min: [min],
                        max: [max]
                    }
                });
                range.noUiSlider.on('update', (values, handle) => {
                    fields[handle].value = values[handle];
                });
            }
        }
    }
};

module.exports = slider;