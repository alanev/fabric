module.exports = {
    image: document.querySelector('.gallery__image'),
    previews: document.querySelectorAll('.gallery__preview'),
    init () {
        let self = this;
        if (this.image) {
            this.previews.forEach(preview => {
                preview.addEventListener('click', e => {
                    self.image.src = preview.dataset.image;
                });
            });
        }
    }
}