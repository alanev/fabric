var popup = {
	activeClass: '_open',
	open: function (popupName) {
		document.querySelector(`.popup[data-popup=${popupName}]`).classList.add(this.activeClass);
	},
	close: function () {
		document.querySelector(`.popup.${this.activeClass}`).classList.remove(this.activeClass);
	},
	init () {
		window.popup = popup;
	}
};
module.exports = popup;