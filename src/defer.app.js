NodeList.prototype.forEach = Array.prototype.forEach;
Document.prototype.$ = Document.prototype.querySelector;
Element.prototype.$ = Element.prototype.querySelector;
Document.prototype.$$ = Document.prototype.querySelectorAll;
Element.prototype.$$ = Element.prototype.querySelectorAll;
Element.prototype.on = Element.prototype.addEventListener;

var gallery = require('gallery/gallery.js');
var popup = require('popup/popup.js');
var slider = require('slider/slider.js');
var intro = require('intro/intro.js');

gallery.init();
popup.init();
slider.init();
intro.init();